export class InstaLocation {
    public id: number;
    public latitude: number;
    public longitude: number;
    public name: string;

    constructor() {
        this.id = null;
        this.latitude = null;
        this.longitude = null;
        this.name = null;
    }
}

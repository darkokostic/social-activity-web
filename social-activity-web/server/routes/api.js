const express = require('express');
const router = express.Router();
const CONSTANTS = require('../helpers/constants');
const authController = require('../controllers/authController');
const usersController = require('../controllers/usersController');
const instagramController = require('../controllers/instagramController');

// Get users
router.get(CONSTANTS.USERS_API_ENDPOINT, usersController.getUsers);

/*
 * Authenticate User.
 */
router.post(CONSTANTS.AUTHENTICATE_API_ENDPOINT, authController.authenticateUser);

/*
 * Instagram Routes.
 */
router.post(CONSTANTS.INSTAGRAM_COUNTS_API_ENDPOINT, instagramController.saveCounts);

module.exports = router;

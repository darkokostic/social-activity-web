import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from '../../components/home/home.component';
import {AuthInstagramComponent} from '../../components/auth-instagram/auth-instagram.component';
import {DashboardComponent} from '../../components/dashboard/dashboard.component';
import {AuthGuard} from '../../guards/auth.guard';
import {HomeGuard} from '../../guards/home.guard';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/',
        pathMatch: 'full',
        canActivate: [HomeGuard]
    },
    {
        path: '',
        component: HomeComponent,
        canActivate: [HomeGuard]
    },
    {
        path: 'auth-instagram',
        component: AuthInstagramComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: ''
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

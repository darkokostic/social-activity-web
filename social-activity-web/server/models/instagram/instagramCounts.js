const mongoose = require('mongoose');
const CONSTANTS = require('../../helpers/constants');
const Schema = mongoose.Schema;

const InstagramCountsSchema = new Schema({
    user_id: {
        type: Number,
        required: [true, CONSTANTS.INSTAGRAM_COUNTS_FOLLOWED_BY_REQUIRED_MSG]
    },
    followed_by: {
        type: Number,
        required: [true, CONSTANTS.INSTAGRAM_COUNTS_FOLLOWED_BY_REQUIRED_MSG]
    },
    follows: {
        type: Number,
        required: [true, CONSTANTS.INSTAGRAM_COUNTS_FOLLOWS_REQUIRED_MSG]
    },
    media: {
        type: Number,
        required: [true, CONSTANTS.INSTAGRAM_COUNTS_MEDIA_REQUIRED_MSG]
    },
    created_date: {
        type: Number,
        required: [true, CONSTANTS.INSTAGRAM_COUNTS_CREATED_DATE_REQUIRED_MSG]
    },
});

const InstagramCounts = mongoose.model('instagramCounts', InstagramCountsSchema);

module.exports = InstagramCounts;

export enum ApiRoutes {
    API_ENDPOINT = 'http://localhost:3000/api/',

    // Instagram
    API_INSTAGRAM_USER = 'https://api.instagram.com/v1/users/self?access_token=',
    API_INSTAGRAM_MEDIA = 'https://api.instagram.com/v1/users/self/media/recent?access_token=',

    // Twitter
    API_TWITTER_REQUEST_TOKEN = 'https://api.twitter.com/oauth/request_token',

    // Authenticate USER
    AUTHENTICATE_USER = 'authenticate',
    INSTAGRAM_COUNTS_USER_API = 'counts',
}

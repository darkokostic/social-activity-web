import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {InstagramService} from '../../services/instagram.service';
import {Constants} from '../../helpers/constants/constants.enum';
import {SocialType} from '../../models/global/social-type';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth-instagram.component.html',
    styleUrls: ['./auth-instagram.component.css']
})
export class AuthInstagramComponent implements OnInit {

    constructor(private activatedRoute: ActivatedRoute, public instagramService: InstagramService, private router: Router, public authService: AuthService) {

        this.activatedRoute.fragment.subscribe((params: any) => {
            const accessToken = params.split('=')[1];

            this.instagramService.getUser(accessToken)
                .subscribe((response) => {
                    const socialType = new SocialType();
                    socialType.name = Constants.INSTAGRAM_TYPE;

                    localStorage.setItem(Constants.TOKEN_LOCALSTORAGE_KEY, accessToken);
                    localStorage.setItem(Constants.SOCIAL_TYPE_LOCALSTORAGE_KEY, JSON.stringify(socialType));
                    localStorage.setItem(Constants.USER_LOCALSTORAGE_KEY, JSON.stringify(response.data));

                    const user = response.data;
                    user.provider = Constants.INSTAGRAM_TYPE;

                    // this.router.navigate([Constants.DASHBOARD_ROUTE]);

                    this.authService.authenticateUser(user)
                        .subscribe((resp) => {
                            console.log(resp);
                            this.router.navigate([Constants.DASHBOARD_ROUTE]);
                        }, (err) => {
                            console.log(err);
                        });
                });
        });
    }


    ngOnInit() {
    }

}

const mongoose = require('mongoose');
const CONSTANTS = require('../helpers/constants');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    provider_id: {
        type: String,
        required: [true, CONSTANTS.USER_PROVIDER_ID_FIELD_REQUIRED_MSG]
    },
    provider: {
        type: String,
        required: [true, CONSTANTS.USER_PROVIDER_FIELD_REQUIRED_MSG]
    },
    name: {
        type: String,
        required: [true, CONSTANTS.USER_NAME_FIELD_REQUIRED_MSG]
    }
});

const User = mongoose.model('users', UserSchema);

module.exports = User;

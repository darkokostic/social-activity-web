import {Component, OnInit} from '@angular/core';
import {Constants} from '../../helpers/constants/constants.enum';
import {InstaUser} from '../../models/instagram/insta-user';
import {SocialType} from '../../models/global/social-type';
import {FbUser} from '../../models/facebook/fb-user';
import {Router} from '@angular/router';
import {InstagramService} from '../../services/instagram.service';
import {InstaPost} from '../../models/instagram/insta-post';
import {InstaMostRepeatedTag} from '../../models/instagram/insta-most-repeated-tag';
import {InstaMostRepeatedFilter} from '../../models/instagram/insta-most-repeated-filter';
import * as chart from 'chart.js';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    public instaUser: InstaUser;
    public fbUser: FbUser;
    public socialType: SocialType;
    public Constants = Constants;
    public posts: InstaPost[];
    public mostLikesPost: InstaPost;
    public mostCommentsPost: InstaPost;
    public allPostsTime: any = 0;
    public averagePostTime: any = 0;
    public averagePostTimeFormatted: string;
    public allTags: string[];
    public allUniqueTags: string[];
    public mostRepeatedTag: InstaMostRepeatedTag;
    public allFilters: string[];
    public mostRepeatedFilter: InstaMostRepeatedFilter;
    // public title: string = 'My first AGM project';
    public lat: any = 51.678418;
    public lng: any = 7.809007;

    // Chart Data
    public followedByChartData: number[] = [];
    public followsChartData: number[] = [];
    public mediaChartData: number[] = [];
    public countsDatesChartData: string[] = [];

    // Followers Chart
    public followersChartData: Array<any>;
    public postsChartData: Array<any>;

    public countsChartLabels: Array<any>;

    public lineChartOptions: any = {
        responsive: true
    };

    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: '#cd9feb75',
            borderColor: '#883cba',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: '#828aff7d',
            borderColor: '#505bd5',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];

    constructor(private router: Router, public instagramService: InstagramService) {
        this.socialType = new SocialType();
        this.socialType = JSON.parse(localStorage.getItem(Constants.SOCIAL_TYPE_LOCALSTORAGE_KEY));
        this.initUser();
    }

    ngOnInit() {
    }

    initUser(): void {
        this.instaUser = new InstaUser();
        this.fbUser = new FbUser();

        switch (this.socialType.name) {
            case Constants.INSTAGRAM_TYPE:
                this.instaUser = JSON.parse(localStorage.getItem(Constants.USER_LOCALSTORAGE_KEY));
                this.getMedia();
                this.saveCounts();
                break;
            case Constants.FACEBOOK_TYPE:
                this.fbUser = JSON.parse(localStorage.getItem(Constants.USER_LOCALSTORAGE_KEY));
                break;
        }
    }

    getMedia(): void {
        this.mostLikesPost = new InstaPost();
        this.mostCommentsPost = new InstaPost();
        this.allTags = [];
        this.allUniqueTags = [];
        this.allFilters = [];
        this.mostRepeatedTag = new InstaMostRepeatedTag();
        this.mostRepeatedFilter = new InstaMostRepeatedFilter();
        this.posts = [];

        this.instagramService.getMedia(localStorage.getItem(Constants.TOKEN_LOCALSTORAGE_KEY))
            .subscribe((response) => {
                this.posts = response.data;
                this.mostLikesPost = response.data[0];
                this.mostCommentsPost = response.data[0];

                response.data.forEach((post) => {
                    if (post.likes.count >= this.mostLikesPost.likes.count) {
                        this.mostLikesPost = post;
                    }

                    if (post.comments.count >= this.mostCommentsPost.comments.count) {
                        this.mostCommentsPost = post;
                    }

                    post.tags.forEach((tag) => {
                        this.allTags.push(tag);
                    });

                    this.allFilters.push(post.filter);
                });

                this.getMostRepeatedTag(this.allTags);

                this.getMostRepeatedFilter(this.allFilters);

                this.removeDuplicateTags(this.allTags, this.allUniqueTags);

                this.countAvergePostTime(response.data);

                this.formatMostPopularPostsDate();

            }, (error) => {
                console.log(error);
            });
    }

    logout(): void {
        localStorage.removeItem(Constants.TOKEN_LOCALSTORAGE_KEY);
        this.router.navigate([Constants.HOME_ROUTE]);
    }

    formatMostPopularPostsDate() {
        const mostLikesPostCreatedDate = new Date(Number(this.mostLikesPost.created_time) * 1000);
        const mostCommentsPostCreatedDate = new Date(Number(this.mostCommentsPost.created_time) * 1000);

        this.mostLikesPost.created_time = mostLikesPostCreatedDate.getDate() + '.' + (mostLikesPostCreatedDate.getMonth() + 1) + '.' + mostLikesPostCreatedDate.getFullYear();
        this.mostCommentsPost.created_time = mostCommentsPostCreatedDate.getDate() + '.' + (mostCommentsPostCreatedDate.getMonth() + 1) + '.' + mostCommentsPostCreatedDate.getFullYear();
    }

    countAvergePostTime(data): void {
        this.allPostsTime = Number(data[0].created_time) - Number(data[data.length - 1].created_time);
        this.averagePostTime = this.allPostsTime / this.posts.length;

        const days = Math.floor(this.averagePostTime / (3600 * 24));
        this.averagePostTime -= days * 3600 * 24;

        const hrs = Math.floor(this.averagePostTime / 3600);
        this.averagePostTime -= hrs * 3600;

        const minutes = Math.floor(this.averagePostTime / 60);

        this.averagePostTimeFormatted = days + ' Days, ' + hrs + ' Hrs, ' + minutes + ' Minutes';
    }

    getMostRepeatedTag(allTags): void {
        if (allTags.length === 0) {
            return;
        }

        const tagsMap = {};
        let mostRepeatedTag = allTags[0];
        let repeatingCount = 1;

        for (let i = 0; i < allTags.length; i++) {
            const tag = allTags[i];
            if (tagsMap[tag] == null) {
                tagsMap[tag] = 1;
            } else {
                tagsMap[tag]++;
            }

            if (tagsMap[tag] > repeatingCount) {
                mostRepeatedTag = tag;
                repeatingCount = tagsMap[tag];
            }
        }

        this.mostRepeatedTag.tag = mostRepeatedTag;
        this.mostRepeatedTag.count = repeatingCount;
    }

    getMostRepeatedFilter(allFilters): void {
        if (allFilters.length === 0) {
            return;
        }

        const filtersMap = {};
        let mostRepeatedFilter = allFilters[0];
        let repeatingCount = 1;

        for (let i = 0; i < allFilters.length; i++) {
            const filter = allFilters[i];
            if (filtersMap[filter] == null) {
                filtersMap[filter] = 1;
            } else {
                filtersMap[filter]++;
            }

            if (filtersMap[filter] > repeatingCount) {
                mostRepeatedFilter = filter;
                repeatingCount = filtersMap[filter];
            }
        }

        this.mostRepeatedFilter.filter = mostRepeatedFilter;
        this.mostRepeatedFilter.count = repeatingCount;
    }

    removeDuplicateTags(allTags, uniqueTags): void {
        allTags.forEach((tag) => {
            if (!uniqueTags.includes(tag)) {
                uniqueTags.push(tag);
            }
        });
    }

    saveCounts(): void {
        this.instagramService.saveCounts()
            .subscribe((response) => {
                response.entity.forEach((countData) => {
                    this.followedByChartData.push(countData.followed_by);
                    this.followsChartData.push(countData.follows);
                    this.mediaChartData.push(countData.media);

                    const created_date = new Date(countData.created_date);
                    const formattedDate = created_date.getDate() + '.' + (created_date.getMonth() + 1) + '.' + created_date.getFullYear() + ' ' + created_date.getHours() + ':' + created_date.getMinutes() + ':' + created_date.getSeconds();
                    this.countsDatesChartData.push(formattedDate);

                    this.countsChartLabels = this.countsDatesChartData;

                    this.followersChartData = [
                        {data: this.followedByChartData, label: 'Followed by'},
                        {data: this.followsChartData, label: 'Follows'},
                    ];

                    this.postsChartData = [
                        {data: this.mediaChartData, label: 'Posts'}
                    ];
                });
            });
    }

    goLink(link: string): void {
        console.log(link);
        window.location.href = link;
    }
}

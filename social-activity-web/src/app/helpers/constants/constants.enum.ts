export enum Constants {
    // Instagram Login Constants
    IN_CLIENT_ID = 'abae86218c7741fba58ec22a16628d42',
    IN_CLIENT_SECRET = '6b6131b9e9ff40c3ad697604794807c1',
    IN_REDIRECT_URL = 'http://localhost:3000/auth-instagram',

    // Facebook Login Constants
    FB_CLIENT_ID = '385802961939470',

    // Twitter Login Constants
    TW_CLIENT_ID = '',
    TW_REDIRECT_URL = 'http://localhost:3000/auth-twitter',

    // Router navigate routes
    DASHBOARD_ROUTE = '/dashboard',
    HOME_ROUTE = '/',

    // Localstorage Keys
    USER_LOCALSTORAGE_KEY = 'user',
    SOCIAL_TYPE_LOCALSTORAGE_KEY = 'social_type',
    TOKEN_LOCALSTORAGE_KEY = 'token',

    // Social Types
    INSTAGRAM_TYPE = 'instagram',
    FACEBOOK_TYPE = 'facebook',
}

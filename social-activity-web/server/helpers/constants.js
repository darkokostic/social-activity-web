const CONSTANTS = {
    // API routes
    USERS_API_ENDPOINT: '/users',
    AUTHENTICATE_API_ENDPOINT: '/authenticate',
    INSTAGRAM_COUNTS_API_ENDPOINT: '/counts',

    // User Auth Messages
    SUCCESS_USER_SAVE_MSG: 'Successfully saved user account',
    SUCCESS_USER_GET_MSG: 'Successfully get user account data',

    // Instagram Counts Messages
    SUCCESS_INSTAGRAM_COUNTS_GET_MSG: 'Successfully get instagram counts data',


    // User Model Messages
    USER_PROVIDER_ID_FIELD_REQUIRED_MSG: 'Provider ID field required',
    USER_PROVIDER_FIELD_REQUIRED_MSG: 'Provider field required',
    USER_NAME_FIELD_REQUIRED_MSG: 'Name field required',

    // Instagram Counts Model Messages
    INSTAGRAM_COUNTS_USER_ID_REQUIRED_MSG: 'user_id field required',
    INSTAGRAM_COUNTS_FOLLOWED_BY_REQUIRED_MSG: 'Followed By field required',
    INSTAGRAM_COUNTS_FOLLOWS_REQUIRED_MSG: 'Follows field required',
    INSTAGRAM_COUNTS_MEDIA_REQUIRED_MSG: 'Media field required',
    INSTAGRAM_COUNTS_CREATED_DATE_REQUIRED_MSG: 'Created Date field required',

    // Providers Name
    INSTAGRAM_PROVIDER_NAME: 'instagram',
    FACEBOOK_PROVIDER_NAME: 'facebook',
};

module.exports = CONSTANTS;

import {InstaCounts} from './insta-counts';

export class InstaUser {
    public id: string;
    public bio: string;
    public full_name: string;
    public is_business: boolean;
    public profile_picture: string;
    public username: string;
    public website: string;
    public counts: InstaCounts;

    constructor() {
        this.id = null;
        this.bio = null;
        this.full_name = null;
        this.is_business = null;
        this.profile_picture = null;
        this.username = null;
        this.website = null;
        this.counts = new InstaCounts();
    }
}

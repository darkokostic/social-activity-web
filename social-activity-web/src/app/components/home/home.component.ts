import {Component, OnInit} from '@angular/core';
import {Constants} from '../../helpers/constants/constants.enum';
import {AuthService, FacebookLoginProvider} from 'angular5-social-login';
import {SocialType} from '../../models/global/social-type';
import {Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    constructor(private socialAuthService: AuthService, private router: Router) {
    }

    ngOnInit() {
    }

    instaLogin(): void {
        window.location.href = 'https://api.instagram.com/oauth/authorize/?client_id=' + Constants.IN_CLIENT_ID + '&redirect_uri=' + Constants.IN_REDIRECT_URL + '&response_type=token&scope=public_content';
    }

    fbLogin(): void {
        this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
            (userData) => {
                console.log(userData);
                const socialType = new SocialType();
                socialType.name = Constants.FACEBOOK_TYPE;

                localStorage.setItem(Constants.TOKEN_LOCALSTORAGE_KEY, userData.token);
                localStorage.setItem(Constants.SOCIAL_TYPE_LOCALSTORAGE_KEY, JSON.stringify(socialType));
                localStorage.setItem(Constants.USER_LOCALSTORAGE_KEY, JSON.stringify(userData));
                this.router.navigate([Constants.DASHBOARD_ROUTE]);
            }
        );
    }
}

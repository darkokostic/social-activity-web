import {InstaImage} from './insta-image';

export class InstaImages {
    public low_resolution: InstaImage;
    public standard_resolution: InstaImage;
    public thumbnail: InstaImage;


    constructor() {
        this.low_resolution = new InstaImage();
        this.standard_resolution = new InstaImage();
        this.thumbnail = new InstaImage();
    }
}

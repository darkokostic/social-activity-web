import {InstaImages} from './insta-images';
import {InstaLikesComments} from './insta-likes-comments';
import {InstaLocation} from './insta-location';
import {InstaCaption} from './insta-caption';
import {InstaUser} from './insta-user';

export class InstaPost {
    public id: string;
    public comments: InstaLikesComments;
    public created_time: string;
    public filter: string;
    public images: InstaImages;
    public likes: InstaLikesComments;
    public link: string;
    public location: InstaLocation;
    public tags: any[];
    public caption: InstaCaption;
    public users_in_photo: InstaUser[];


    constructor() {
        this.id = null;
        this.comments = new InstaLikesComments();
        this.created_time = null;
        this.filter = null;
        this.images = new InstaImages();
        this.likes = new InstaLikesComments();
        this.link = null;
        this.location = new InstaLocation();
        this.tags = [];
        this.caption = new InstaCaption();
        this.users_in_photo = [];
    }
}

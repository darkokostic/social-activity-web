import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {HttpClient} from '@angular/common/http';
import {ApiRoutes} from '../helpers/constants/api-routes.enum';
import {Constants} from '../helpers/constants/constants.enum';

@Injectable()
export class InstagramService {

    constructor(public http: HttpClient) {
    }

    getUser(token: string): Observable<any> {
        return this.http.get(ApiRoutes.API_INSTAGRAM_USER + token)
            .map((res: Response) => res)
            .catch((err: any) => {
                return Observable.throw(err);
            });
    }

    getMedia(token: string): Observable<any> {
        return this.http.get(ApiRoutes.API_INSTAGRAM_MEDIA + token)
          .map((res: Response) => res)
          .catch((err: any) => {
              return Observable.throw(err);
          });
    }

    saveCounts(): Observable<any> {
        const userLocalStorage = JSON.parse(localStorage.getItem(Constants.USER_LOCALSTORAGE_KEY));
        const counts = userLocalStorage.counts;
        counts.created_date = Math.round(new Date().getTime());
        console.log(counts.created_date);
        counts.user_id = userLocalStorage.id;

        return this.http.post(ApiRoutes.API_ENDPOINT + ApiRoutes.INSTAGRAM_COUNTS_USER_API, {counts: counts})
            .map((res: Response) => res)
            .catch((err: any) => {
                return Observable.throw(err);
            });
    }
}

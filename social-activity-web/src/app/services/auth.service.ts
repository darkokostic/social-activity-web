import {Injectable} from '@angular/core';
import {Constants} from '../helpers/constants/constants.enum';
import {ApiRoutes} from '../helpers/constants/api-routes.enum';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class AuthService {

    constructor(public http: HttpClient) {
    }

    isLoggedIn(): boolean {
        const token = localStorage.getItem(Constants.TOKEN_LOCALSTORAGE_KEY);
        return token !== null;
    }

    authenticateUser(user: any): Observable<any> {
        return this.http.post(ApiRoutes.API_ENDPOINT + ApiRoutes.AUTHENTICATE_USER, {user: user})
            .map((res: Response) => res)
            .catch((err: any) => {
                return Observable.throw(err);
            });
    }

}

const express = require('express');
const CONSTANTS = require('../helpers/constants');
const User = require('../models/user');

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    message: null,
    entity: [],
};

module.exports.getUsers = (req, res) => {
    User.find({})
        .then((data) => {
            response.status = 200;
            response.entity = data;
            response.message = CONSTANTS.SUCCESS_USER_GET_MSG;
            res.json(response);
        });
};
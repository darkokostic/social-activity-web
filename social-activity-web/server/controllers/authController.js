const CONSTANTS = require('../helpers/constants');
const User = require('../models/user');

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    message: null,
    entity: [],
};

module.exports.authenticateUser = (req, res) => {
    const userParams = req.body.user;
    let user = null;

    User.find({ provider_id: userParams.id, provider: userParams.provider })
        .then((data) => {
            if (!data.length) {
                console.log("NO DATA");
                if (userParams.provider === CONSTANTS.INSTAGRAM_PROVIDER_NAME) {
                    user = {
                        provider_id: userParams.id,
                        provider: userParams.provider,
                        name: userParams.full_name
                    };
                    User.create(user, (error) => {
                        if (error) return sendError(error, res);
                    })
                }

                response.status = 200;
                response.entity = user;
                response.message = CONSTANTS.SUCCESS_USER_GET_MSG;
                res.json(response);

            } else {
                console.log("HAS DATA");
                response.status = 200;
                response.entity = data;
                response.message = CONSTANTS.SUCCESS_USER_GET_MSG;
                res.json(response);
            }
        });
};
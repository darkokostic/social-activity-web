import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './helpers/routing/app-routing.module';
import {HomeComponent} from './components/home/home.component';
import {AuthInstagramComponent} from './components/auth-instagram/auth-instagram.component';
import {InstagramService} from './services/instagram.service';
import {HttpClientModule} from '@angular/common/http';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AuthServiceConfig, FacebookLoginProvider, SocialLoginModule} from 'angular5-social-login';
import {Constants} from './helpers/constants/constants.enum';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './guards/auth.guard';
import {HomeGuard} from './guards/home.guard';
import {ChartsModule} from 'ng2-charts';
import { AgmCoreModule } from '@agm/core';

// Configs
export function getAuthServiceConfigs() {
    const config = new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider(Constants.FB_CLIENT_ID)
            },
            // {
            //     id: GoogleLoginProvider.PROVIDER_ID,
            //     provider: new GoogleLoginProvider('')
            // }
        ]
    );
    return config;
}

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AuthInstagramComponent,
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        SocialLoginModule,
<<<<<<< HEAD
        ChartsModule
=======
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBNm3o8a5GoyQ-YE58ZR90ivQqcRpkGKqc&callback'
        })
>>>>>>> afcd9ccfdd81edfcb7d3324e1ef3557c25bbdd2a
    ],
    providers: [InstagramService, {
        provide: AuthServiceConfig,
        useFactory: getAuthServiceConfigs
    }, AuthService, AuthGuard, HomeGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}

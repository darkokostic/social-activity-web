export class InstaCounts {
    public followed_by: number;
    public follows: number;
    public media: number;


    constructor() {
        this.followed_by = null;
        this.follows = null;
        this.media = null;
    }
}

export class InstaImage {
    public height: number;
    public width: number;
    public url: string;


    constructor() {
        this.height = null;
        this.width = null;
        this.url = null;
    }
}

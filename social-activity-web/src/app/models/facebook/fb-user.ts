export class FbUser {
    public image: string;
    public name: string;
    public email: string;

    constructor() {
        this.image = null;
        this.name = null;
        this.email = null;
    }
}

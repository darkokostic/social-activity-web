const CONSTANTS = require('../helpers/constants');
const InstagramCounts = require('../models/instagram/instagramCounts');

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    message: null,
    entity: [],
};

module.exports.saveCounts = (req, res) => {
    const countsParams = req.body.counts;
    let newInstagramCounts = {
        user_id: countsParams.user_id,
        followed_by: countsParams.followed_by,
        follows: countsParams.follows,
        media: countsParams.media,
        created_date: countsParams.created_date
    };

    InstagramCounts.find(newInstagramCounts)
        .then((data) => {
            if (!data.length) {
                InstagramCounts.create(newInstagramCounts, (error) => {
                    if (error) return sendError(error, res);
                }, () => {
                    InstagramCounts.find({user_id: countsParams.user_id})
                        .then((data) => {
                            response.status = 200;
                            response.entity = data;
                            response.message = CONSTANTS.SUCCESS_INSTAGRAM_COUNTS_GET_MSG;
                            res.json(response);
                        });
                })
            } else {
                InstagramCounts.find({user_id: countsParams.user_id})
                    .then((data) => {
                        response.status = 200;
                        response.entity = data;
                        response.message = CONSTANTS.SUCCESS_INSTAGRAM_COUNTS_GET_MSG;
                        res.json(response);
                    });
            }
        });
};